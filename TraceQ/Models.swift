//
//  Models.swift
//  TraceQ
//
//  Created by Danilo Casillo on 12/05/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class Position : NSObject{
    var citta : String!
    var via : String!
    var url : String!
  /*  var latitude: CLLocationDegrees!
    var longitude: CLLocationDegrees!
   */
    init(citta : String, via: String, url: String){
        self.citta = citta
        self.via = via
        self.url = url
    }
    
   /* func getMapItem() -> MKMapItem {
        let coordinates = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let placeMark = MKPlacemark(coordinate: coordinates,addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placeMark)
        self.citta = placeMark.locality
        self.via = placeMark.subLocality
        return mapItem
    }*/
    
    
}

class Activity: NSObject {
    var code : String!
    var name : String!
    var image : String!
    var position : Position!
    var type : String!
    
    init(code: String, name: String,image: String, type: String, citta: String, via: String, url: String){
        self.image = image
        self.code = code
        self.type = type
        self.name = name
        self.position = Position(citta: citta, via: via, url: url)
    }
    
    func openPosition(){
        if let url = URL(string: self.position.url)
        {
             UIApplication.shared.open(url)
        }
    }
}
