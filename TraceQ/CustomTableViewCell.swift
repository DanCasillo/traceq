//
//  CustomTableViewCell.swift
//  TraceQ
//
//  Created by Michele Garofalo on 14/05/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    
    @IBOutlet weak var ResturantImage:UIImageView!
    @IBOutlet weak var ResturantName: UILabel!
    @IBOutlet weak var ResturantAddress: UILabel!
    @IBOutlet weak var MapButton: UIButton!
    @IBOutlet weak var ClockPic: UIImageView!
    @IBOutlet weak var Time: UILabel!
    @IBOutlet weak var BookBtn: UIButton!
    
    var isExpandend : Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
    
}
