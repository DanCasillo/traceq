//
//  ViewController.swift
//  TraceQ
//
// It contains all activities (restaurants, pizzerie e pub) 


import UIKit
import Firebase

class ViewController: UIViewController,UIViewControllerTransitioningDelegate{
    
    private var dateCellExpanded: Bool = false
    
    //UI elements
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var QRButton: UIButton!
    var tapped : Bool = false
    
    //Data elements
    var ref: DatabaseReference!
    var storage : StorageReference!
    
    var activities = [Activity]() {
        didSet {
            tableview.reloadData()
        }
    }
    
    var codeActivity : String!
    var nameActivity : String!
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.joinQ), name: Notification.Name("booking"), object: nil)
        /* if let textfield = searchBar.value(forKey: "searchField") as? UITextField {
         textfield.textColor = UIColor.blue
         if let backgroundview = textfield.subviews.first {
         // Background color
         backgroundview.backgroundColor = UIColor.white
         // Rounded corner
         backgroundview.layer.cornerRadius = 14;
         backgroundview.clipsToBounds = true;
         }
         }
         */
        tableview.delegate = self
        tableview.dataSource = self
        tableview.tableFooterView = UIView()
        
        QRButton.CreateFloatingButton()
        
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        ref = Database.database().reference()
        storage = Storage.storage().reference()
        initActivities()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        /*Init navbar*/
        let searchBar = UISearchBar()
        searchBar.sizeToFit()
        searchBar.placeholder = "Search"
        searchBar.searchTextField.backgroundColor = .white
        navigationItem.titleView = searchBar
        
        
        let button = UIButton(type: .custom)
        button.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        button.setImage(UIImage(named: "queueButton"), for: .normal)
        button.addTarget(self, action: #selector(tap), for: .touchUpInside)
        
        let barButton = UIBarButtonItem(customView: button)
        
        barButton.image?.withRenderingMode(.alwaysOriginal)
        if (UserDefaults.standard.bool(forKey: "isBooked"))
        {
            print("vero")
            barButton.customView?.isUserInteractionEnabled = true
            barButton.customView?.alpha = 1
        }
        else
        {
            print("falso")
            barButton.customView?.isUserInteractionEnabled = false
            barButton.customView?.alpha = 0.5
        }
        navigationItem.rightBarButtonItem = barButton
        
    }
    
    @objc func tap(){
        print("tapped")
    }
    
    
    
    @objc func openMaps(sender: UIButton){
        
        print("tap")
        activities[sender.tag].openPosition()
        
    }
    
    @IBAction func openCamera(_ sender: Any) {
        performSegue(withIdentifier: "openCamera", sender: sender)
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "CameraViewController")
//        vc.transitioningDelegate = self
//        vc.modalPresentationStyle = .custom
//        self.present(vc, animated: true)

    }
    @objc func joinQ(_ notification : Notification) {
        let code = notification.userInfo!["Code"] as! String
        
        
        
        if (checkCode(codeDetected: code)){
            self.performSegue(withIdentifier: "booking", sender: self)
        }
        else {
            let alert = UIAlertController(title: "ERROR!", message: "QR code is not correct", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {(action) in
                
            } )
            
            
            alert.addAction(cancelAction)
            
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    
    func checkCode(codeDetected : String) -> Bool  {
        
        /* let reference = Database.database().reference().child("Attività")
         
         var i: Int = 0
         
         reference.observe(.value, with: {snapshot in
         for child in snapshot.children {
         let snap = child as! DataSnapshot
         
         for child in snap.children {
         let snap = child as! DataSnapshot
         if codeActivity == snap.key {
         print("check change")
         self.check = true
         let nameActivity = ((snap.value as! NSDictionary)["Nome"] as! String)
         completion(true, nameActivity)
         }
         }
         }
         
         completion(true,"")
         })*/
        
        for activity in activities {
            if activity.code == codeDetected {
                codeActivity = codeDetected
                nameActivity = activity.name!
                return true
            }
        }
        return false
        
        print("outside")
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "booking" {
            let vc = segue.destination as! JoinQViewController
            vc.nameActivity = nameActivity
            vc.codeActivity = codeActivity
        } else if segue.identifier == "openCamera" {
            let button = sender as! UIButton
            let segue = (segue as! OHCircleSegue)
            segue.destination.modalPresentationStyle = .fullScreen
            segue.circleOrigin = button.center
        }
        

    }
    
    
   
    
    
    
    //There, I have to get the list of all the activities
    func initActivities() {
        
        let parent = ref.child("Attività")
        
        parent.child("Ristoranti").observe(.childAdded, with: {snapshot in
            
            let p = snapshot.value as? NSDictionary
            print("Rist Codice: \(snapshot.key)")
            
            let ristorante = Activity(code: snapshot.key, name: p!["Nome"] as! String,image: p!["Immagine"] as! String,type: "Ristoranti",citta: p!["Citta"] as! String,via: p!["Via"] as! String , url: p!["URL"] as! String)
            
            self.activities.append(ristorante)
            
        })
        
        parent.child("Pizzerie").observe(.childAdded, with: {snapshot in
            
            let p = snapshot.value as? NSDictionary
            print("Pizz Codice: \(snapshot.key)")
            
            let pizzeria = Activity(code: snapshot.key, name: p!["Nome"] as! String,image: p!["Immagine"] as! String,type: "Pizza",citta: p!["Citta"] as! String,via: p!["Via"] as! String , url: p!["URL"] as! String)
            
            self.activities.append(pizzeria)
            
        })
        
       
         
        
    }
    
    
}



extension ViewController : UITableViewDataSource,UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return activities.count
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tapped {
            guard let cell = tableView.cellForRow(at: indexPath) as? CustomTableViewCell else {
                return 185
            }
            
            if cell.isExpandend {
                return 255
            } else {
                return 185
            }
            
        }
        
        
        return 185
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! CustomTableViewCell
        tapped = true
        if cell.isExpandend {
            cell.isExpandend = false
        } else {
            cell.isExpandend = true
        }
        tableview.beginUpdates()
        tableView.endUpdates()
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "custom cell") as! CustomTableViewCell
        
        
        cell.layer.cornerRadius = cell.frame.height/6
        
        cell.ResturantImage.layer.cornerRadius = cell.ResturantImage.frame.height/6
        cell.ResturantImage.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        let url = activities[indexPath.section].image!
        let image = storage.child("images").child(url)
        
        image.getData(maxSize: 4*1024*1024, completion: {(data,error) in
            if let error = error {
                print(error)
                return
            }
            if let data = data {
                cell.ResturantImage.image = UIImage(data: data)
            }
        })
        
        cell.MapButton.tag = indexPath.section
        cell.MapButton.addTarget(self, action: #selector(openMaps), for: .touchUpInside)
        cell.ResturantImage.clipsToBounds = true
        cell.ResturantName.text = activities[indexPath.section].name!
        
        return cell
    }
}

extension UIButton {
    
    func CreateFloatingButton () {
        layer.cornerRadius = frame.height/2
        layer.shadowOpacity = 0.25
        layer.shadowRadius = 5
        layer.shadowOffset = CGSize(width: 0, height: 10)
        
    }
}
