////
////  BookingViewController.swift
////  TraceQ
////
////  Created by Danilo Casillo on 12/05/2020.
////  Copyright © 2020 Danilo Casillo. All rights reserved.
////
//
//import UIKit
//import Firebase
//
//class BookingViewController: UIViewController {
//
//    //UI Elements
//    @IBOutlet weak var peoplesLabel: UILabel!
//    @IBOutlet weak var turnLabel: UILabel!
//
//
//
//    override func viewDidLoad() {
//
//        super.viewDidLoad()
//        ref = Database.database().reference()
//        NotificationCenter.default.addObserver(self, selector: #selector(checkBookings), name: Notification.Name(rawValue: "turnShowed"), object: nil)
//    }
//
//    @IBAction func changeValue(_ sender: UIStepper) {
//        peoplesLabel.text = String(sender.value)
//        print("changeValue")
//
//        getBookings()
//
//        NotificationCenter.default.post(name: Notification.Name("turnShowed"), object: nil)
//    }
//
//    @objc func checkBookings(){
//        print("book")
//
//    }
//}
//
//extension BookingViewController {
//
//    func getBookings(){
//
//        var totalBookings : Int = 0 {
//            didSet{
//                self.turnLabel.text = String(totalBookings+1)
//            }
//        }
//
//        ref.child("Prenotazione").child(codeActivity).observe(.childAdded, with: {snapshot in
//            print(snapshot)
//            let booking = snapshot.value as! NSDictionary
//            if (booking["Numero"] as! Int == Int(self.peoplesLabel.text!)){
//                totalBookings += 1
//            }
//        })
//
//    }
//}
//
